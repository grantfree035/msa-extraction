import csv
from collections import OrderedDict


def list2dict(keyfunction, values):
    return dict((keyfunction(v), v) for v in values)


cbsaFilename = '../data/ref/cbsa_fips_2015_nber.csv'
birthFilename = '../data/clean/birth_2015_county_cdc_15-19.csv'
crimeFilename = '../data/clean/crime_2014_county_ucrpd.csv'
popFilename = '../data/clean/population_2014_county_pepannres.csv'
povertyFilename = '../data/clean/2015_poverty_county_census.csv'
metroFilename = '../data/merge/metro_2014_2015.csv'

# ingest data from input csv's
cbsaFips = list(csv.DictReader(open(cbsaFilename)))
countyBirth = list(csv.DictReader(open(birthFilename)))
countyPop = list(csv.DictReader(open(popFilename)))
countyCrime = list(csv.DictReader(open(crimeFilename)))
countyPoverty = list(csv.DictReader(open(povertyFilename)))

# Map csv list to dict
# k = FIPS County Code
# v = OrderedDict of row
countyBirthDict = list2dict(lambda a: a['FIPS County Code'], countyBirth)
countyPopDict = list2dict(lambda a: a['GEO.id2'], countyPop)
countyCrimeDict = list2dict(lambda a: a['FIPS_ST'].zfill(2) + a['FIPS_CTY'].zfill(3), countyCrime)
countyPovertyDict = list2dict(lambda a: a['County ID'], countyPoverty)

# Create writer for output csv
metronames = ['cbsa',
              'cbsa_title']
popnames = ['population']
crimenames = ['total_crimes',
              'violent_crimes',
              'property_crimes',
              'murder',
              'rape',
              'robbery',
              'aggravated_assault',
              'burglary',
              'larceny',
              'mv_theft',
              'arson']
birthnames = ['teen_births',
              'crime_coverage']
povertynames = ['All Ages SAIPE Poverty Universe',
                'All Ages in Poverty Count',
                'All Ages in Poverty Count LB 90%',
                'All Ages in Poverty Count UB 90%',
                # '90% Confidence Interval (All Ages in Poverty Count)',
                # 'All Ages in Poverty Percent',
                # 'All Ages in Poverty Percent LB 90%',
                # 'All Ages in Poverty Percent UB 90%',
                # '90% Confidence Interval (All Ages in Poverty Percent)',
                'Under Age 18 SAIPE Poverty Universe',
                'Under Age 18 in Poverty Count',
                'Under Age 18 in Poverty Count LB 90%',
                'Under Age 18 in Poverty Count UB 90%',
                # '90% Confidence Interval (Under Age 18 in Poverty Count)',
                # 'Under Age 18 in Poverty Percent',
                # 'Under Age 18 in Poverty Percent LB 90%',
                # 'Under Age 18 in Poverty Percent UB 90%',
                # '90% Confidence Interval (Under Age 18 in Poverty Percent)',
                'Ages 5 to 17 in Families SAIPE Poverty Universe',
                'Ages 5 to 17 in Families in Poverty Count',
                'Ages 5 to 17 in Families in Poverty Count LB 90%',
                'Ages 5 to 17 in Families in Poverty Count UB 90%',
                # '90% Confidence Interval (Ages 5 to 17 in Families in Poverty Count)',
                # 'Ages 5 to 17 in Families in Poverty Percent',
                # 'Ages 5 to 17 in Families in Poverty Percent LB 90%',
                # 'Ages 5 to 17 in Families in Poverty Percent UB 90%',
                # '90% Confidence Interval (Ages 5 to 17 in Families in Poverty Percent)',
                'Under Age 5 SAIPE Poverty Universe',
                'Under Age 5 in Poverty Count',
                'Under Age 5 in Poverty Count LB 90%',
                'Under Age 5 in Poverty Count UB 90%',
                # '90% Confidence Interval (Under Age 5 in Poverty Count)',
                # 'Under Age 5 in Poverty Percent',
                # 'Under Age 5 in Poverty Percent LB 90%',
                # 'Under Age 5 in Poverty Percent UB 90%',
                # '90% Confidence Interval (Under Age 5 in Poverty Percent)',
                'Median Household Income in Dollars',
                'Median Household Income in Dollars LB 90%',
                'Median Household Income in Dollars UB 90%',
                # '90% Confidence Interval (Median Household Income in Dollars)'
                ]
coverage = ['poverty_coverage',
            'teen_birth_coverage',
            'crime_coverage']
fieldnames = metronames + popnames + crimenames + birthnames + povertynames + coverage

metroFile = open(metroFilename, 'w', newline='', encoding='UTF-8')
writer = csv.DictWriter(metroFile, fieldnames=fieldnames)
writer.writeheader()

# Generate ordered cbsa dictionary
# k = cbsa code
# v = cbsa title
cbsaDict = {}
for county in cbsaFips:
    # skip territories
    if county['fipsstatecode'] not in ['72']:
        cbsaDict[county['cbsacode']] = county['cbsatitle']
cbsaDict = OrderedDict(sorted(cbsaDict.items()))

for cbsa, cbsaTitle in cbsaDict.items():
    # Define new cbsa row (MSA)
    metroRow = dict((f, 0) for f in fieldnames)

    # Add basic cbsa info
    metroRow['cbsa'] = cbsa
    metroRow['cbsa_title'] = cbsaTitle

    # collect FIPS County Codes for cbsa
    cbsaCounties = []
    for county in cbsaFips:
        if cbsa == county['cbsacode']:
            cbsaCounties.append(county['fipsstatecode'] + county['fipscountycode'])

    # Population of specified data was gathered from.
    # will be used for coverage.
    birthPopulation = 0
    crimePopulation = 0

    for fips in cbsaCounties:
        popRow = countyPopDict.get(fips)
        birthRow = countyBirthDict.get(fips)
        crimeRow = countyCrimeDict.get(fips)
        povertyRow = countyPovertyDict.get(fips)

        if popRow is not None:
            pop = popRow['respop72014']
            metroRow['population'] += int(pop)
        else:
            # ignore 51515 (Bedford City), the MSA for 51515 includes Bedford County
            print('missing population for county {}'.format(fips))

        if birthRow is not None:
            metroRow['teen_births'] += int(birthRow['Births'])
            birthPopulation += int(pop)

        if crimeRow is not None:
            metroRow['total_crimes'] += int(crimeRow['VIOL']) + int(crimeRow['PROPERTY'])
            metroRow['violent_crimes'] += int(crimeRow['VIOL'])
            metroRow['property_crimes'] += int(crimeRow['PROPERTY'])
            metroRow['murder'] += int(crimeRow['MURDER'])
            metroRow['rape'] += int(crimeRow['RAPE'])
            metroRow['robbery'] += int(crimeRow['ROBBERY'])
            metroRow['aggravated_assault'] += int(crimeRow['AGASSLT'])
            metroRow['burglary'] += int(crimeRow['BURGLRY'])
            metroRow['larceny'] += int(crimeRow['LARCENY'])
            metroRow['mv_theft'] += int(crimeRow['MVTHEFT'])
            metroRow['arson'] += int(crimeRow['ARSON'])
            crimePopulation += int(pop) * (float(crimeRow['COVIND']) / 100)

        if povertyRow is not None:
            for field in povertynames:
                metroRow[field] += int(povertyRow[field].strip('$').replace(',', '').replace('NA', '0'))

    metroRow['poverty_coverage'] = round(metroRow['All Ages SAIPE Poverty Universe'] / metroRow['population'] * 100, 2)
    metroRow['teen_birth_coverage'] = round(birthPopulation / metroRow['population'] * 100, 2)
    metroRow['crime_coverage'] = round(crimePopulation / metroRow['population'] * 100, 2)

    print('{} {:50s}\tpop={:10d}\tbirthCov={:7.2f}\tcrimeCov={:7.2f}\tpovertyCov={:7.2f}'
          .format(cbsa, cbsaTitle,
                  metroRow['population'], metroRow['teen_birth_coverage'],
                  metroRow['crime_coverage'], metroRow['poverty_coverage']))

    writer.writerow(metroRow)
