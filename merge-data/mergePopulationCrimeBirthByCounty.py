import csv
import locale

birthFilename = "../data/clean/birth_2015_county_cdc_15-19.csv"
populationFilename = "../data/clean/population_2014_county_pepannres.csv"
crimeFilename = "../data/clean/crime_2014_county_ucrpd.csv"
mergeFilename = "../data/merge/county_2014_2015.csv"

countyBirth = list(csv.DictReader(open(birthFilename)))
countyPop = list(csv.DictReader(open(populationFilename)))
countyCrime = list(csv.DictReader(open(crimeFilename)))

fieldnames = ['fips', 'county', 'population', 'teen_births', 'coverage_indicator', 'violent_crimes', 'property_crimes',
              'murder', 'rape', 'robbery', 'aggravatedAssault', 'burglary', 'larceny', 'mv_theft', 'arson']
writer = csv.DictWriter(open(mergeFilename, 'w', newline='', encoding='UTF-8'), fieldnames=fieldnames)
writer.writeheader()

for popRow in population:
    births = ""
    coverageIndicator = ""
    violentCrimes = ""
    propertyCrimes = ""
    murder = ""
    rape = ""
    robbery = ""
    aggravatedAssault = ""
    burglary = ""
    larceny = ""
    motorVehicleTheft = ""
    arson = ""

    fips = popRow['GEO.id2']
    pop = popRow['respop72014']
    county = popRow['GEO.display-label']

    for birthRow in teenBirths:
        if fips == birthRow['FIPS County Code']:
            births = birthRow['Births']
            break

    for crimeRow in crime:
        crimeFips = crimeRow['FIPS_ST'].zfill(2) + crimeRow['FIPS_CTY'].zfill(3)
        if crimeFips == fips:
            coverageIndicator = crimeRow['COVIND']
            violentCrimes = crimeRow['VIOL']
            propertyCrimes = crimeRow['PROPERTY']
            murder = crimeRow['MURDER']
            rape = crimeRow['RAPE']
            robbery = crimeRow['ROBBERY']
            aggravatedAssault = crimeRow['AGASSLT']
            burglary = crimeRow['BURGLRY']
            larceny = crimeRow['LARCENY']
            motorVehicleTheft = crimeRow['MVTHEFT']
            arson = crimeRow['ARSON']
            break

    writer.writerow({'fips': fips, 'county': county, 'population': pop, 'teen_births': births,
                     'coverage_indicator': coverageIndicator, 'violent_crimes': violentCrimes,
                     'property_crimes': propertyCrimes, 'murder': murder, 'rape': rape, 'robbery': robbery,
                     'aggravatedAssault': aggravatedAssault, 'burglary': burglary, 'larceny': larceny,
                     'mv_theft': motorVehicleTheft, 'arson': arson})
