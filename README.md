# MSA Extraction
Scripts for cleaning statistical data and grouping data by MSA.

### Sources
[poverty](https://www.census.gov/data/data-tools/saipe-interactive.html) - Can be downloaded directly to clean data folder.  
[crime](http://www.icpsr.umich.edu/icpsrweb/NACJD/series/00057)  
[birth](https://wonder.cdc.gov/natality-current.html)