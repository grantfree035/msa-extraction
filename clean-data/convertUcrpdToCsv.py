import csv

with open("../data/raw/crime_2014_county_ucrpd.tsv") as raw, \
        open("../data/clean/crime_2014_county_ucrpd.csv", "w", newline='') as clean:

    reader = csv.reader(raw, dialect='excel-tab')
    writer = csv.writer(clean)

    for row in reader:
        writer.writerow(row)
