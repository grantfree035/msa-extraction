rawFilename = "../data/raw/births_2015_county_cdc_15-19.txt"
cleanFilename = "../data/clean/birth_2015_county_cdc_15-19.csv"

cleanFile = open(cleanFilename, "w")

with open(rawFilename) as raw:
    # replace header with new format
    cleanFile.write("County Name,County Type,USPS State Code,FIPS County Code,Births\n")
    next(raw)

    for line in raw:

        # dashes signifies end of data and start of notes
        if "---" in line:
            break

        # Result of split by quotations:
        # 0 - na
        # 1 - county name, county type, usps state code
        # 2 - na
        # 3 - fips state code, fips county code
        # 4 - births
        line = line.split("\"")

        # Parse county name, type and usps state code
        countyUsps = line[1].split(",")
        uspsCode = countyUsps[1].strip()
        countyInfo = countyUsps[0]
        k = countyInfo.rfind(" ")
        countyType = countyInfo[k + 1:]
        countyName = countyInfo[:k]

        # Parse fips codes
        fips = line[3]

        # Parse Births
        births = line[4].strip()

        # write to file
        cleanFile.write("{},{},{},{},{}\n".format(countyName, countyType, uspsCode, fips, births))

cleanFile.close()
